//
//  UIFont+CustomFont.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

extension UIFont {
    static func setCustomFontWith(size: CGFloat) -> UIFont {
        guard
            let customFont = UIFont(name: "OpenSans-Regular", size: size)
        else { return UIFont.systemFont(ofSize: size)}

        return customFont
    }
}
