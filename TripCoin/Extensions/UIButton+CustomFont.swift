//
//  UIButton+CustomFont.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 20.10.22.
//

import Foundation
import UIKit

extension UIButton {
    static func createStandardButton() -> UIButton {
        let button = UIButton(type: .system)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(UIColor.black, for: .normal)
        button.setTitleColor(UIColor.gray, for: .highlighted)
        return button
    }
}

