//
//  UITextField+SecureToggle.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 21.10.22.
//

import Foundation
import UIKit

var button = UIButton(type: .custom)

extension UITextField {
    func enablePasswordToggle() {
        var configuration = UIButton.Configuration.filled()
        configuration.image = UIImage(systemName: "eye.fill")
        configuration.baseBackgroundColor = .clear
        button = UIButton(configuration: configuration)
        button.addTarget(self, action: #selector(togglePassword), for: .touchUpInside)
        button.alpha = 0.7
        rightView = button
        rightViewMode = .always
    }

    @objc
    func togglePassword(_ sender: UIButton) {
        isSecureTextEntry.toggle()
        button.isSelected.toggle()
    }
}
