//
//  RegisterViewConfirmPasswordField.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 24.10.22.
//

import Foundation
import UIKit
import JVFloatLabeledTextField

class RegisterViewConfirmTextField: RegisterViewPasswordTextField {
    func confirmTextFieldConfig(superView: UIView) {
        self.placeholder = "Retype Password"
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
    }
}
