//
//  RegisterViewFieldsStack.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 23.10.22.
//

import Foundation
import UIKit

class RegisterViewFieldsStack: UIStackView {
    
    let fullNameTextField = RegisterViewFullNameTextField()
    let emailTextField = RegisterViewEmailTextField()
    let passwordTextField = RegisterViewPasswordTextField()
    let confirmTextField = RegisterViewConfirmTextField()
    let postCodeTextField = RegisterViewPostCodeTextField()
    let phoneNumberTextField = RegisterViewPhoneNumberTextField()
    
    func configure(superView: UIView) {
        self.axis = .vertical
        self.alignment = .fill
        self.distribution = .fillEqually
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.addArrangedSubview(fullNameTextField)
        fullNameTextField.configure(superView: superView)
        fullNameTextField.addBottomLine()
        fullNameTextField.delegate = fullNameTextField
        
        self.addArrangedSubview(emailTextField)
        emailTextField.configure(superView: superView)
        emailTextField.addBottomLine()
        emailTextField.delegate = emailTextField
        
        self.addArrangedSubview(passwordTextField)
        passwordTextField.configure()
        passwordTextField.setFontSize(superView: superView)
        passwordTextField.addBottomLine()
        passwordTextField.delegate = passwordTextField
        
        self.addArrangedSubview(confirmTextField)
        confirmTextField.configure()
        confirmTextField.setFontSize(superView: superView)
        confirmTextField.confirmTextFieldConfig(superView: superView)
        confirmTextField.addBottomLine()
        confirmTextField.delegate = confirmTextField
        
        self.addArrangedSubview(postCodeTextField)
        postCodeTextField.configure(superView: superView)
        postCodeTextField.addBottomLine()
        postCodeTextField.delegate = postCodeTextField
        
        self.addArrangedSubview(phoneNumberTextField)
        phoneNumberTextField.configure(superView: superView)
        phoneNumberTextField.addBottomLine()
        phoneNumberTextField.delegate = phoneNumberTextField
    }
    
    func setStackConstraints(superView: UIScrollView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: superView.contentLayoutGuide.topAnchor, constant: 0),
            self.leadingAnchor.constraint(equalTo: superView.contentLayoutGuide.leadingAnchor, constant: 0),
            self.trailingAnchor.constraint(equalTo: superView.contentLayoutGuide.trailingAnchor, constant: 0),
            self.bottomAnchor.constraint(equalTo: superView.contentLayoutGuide.bottomAnchor, constant: 0),
            self.widthAnchor.constraint(equalTo: superView.frameLayoutGuide.widthAnchor, multiplier: 1.0)
        ])
    }
}

