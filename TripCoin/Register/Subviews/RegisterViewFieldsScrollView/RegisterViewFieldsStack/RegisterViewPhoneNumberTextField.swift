//
//  RegisterViewPhoneNumberTextField.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 24.10.22.
//

import Foundation
import UIKit
import JVFloatLabeledTextField

class RegisterViewPhoneNumberTextField: RegisterViewFullNameTextField {
    override func configure(superView: UIView) {
        self.placeholder = "Phone Number"
        self.tintColor = .yellow
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
        self.keyboardType = .phonePad
        self.floatingLabelActiveTextColor = .gray
    }
}
