//
//  RegisterViewPostCodeTextField.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 24.10.22.
//

import Foundation
import UIKit
import JVFloatLabeledTextField

class RegisterViewPostCodeTextField: RegisterViewFullNameTextField {
    override func configure(superView: UIView) {
        self.placeholder = "Post Code"
        self.tintColor = .yellow
        self.keyboardType = .numberPad
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
        self.floatingLabelActiveTextColor = .gray
    }
}
