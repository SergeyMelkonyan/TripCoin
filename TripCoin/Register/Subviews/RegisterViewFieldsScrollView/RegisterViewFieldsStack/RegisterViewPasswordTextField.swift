//
//  RegisterViewPasswordTextField.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 24.10.22.
//

import Foundation
import UIKit
import JVFloatLabeledTextField

class RegisterViewPasswordTextField: RegisterViewFullNameTextField {
    
    var button = UIButton(type: .system)
    
    var isEyeTapped = false
    
    func configure() {
        if isEyeTapped == false {
            button.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        }
        self.placeholder = "Password"
        
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.tintColor = .black
        self.rightView = button
        self.tintColor = .yellow
        self.textContentType = .oneTimeCode
        self.rightViewMode = .always
        self.floatingLabelActiveTextColor = .gray
    }
    
    func setFontSize(superView: UIView) {
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
    }
    
    override var isSecureTextEntry: Bool {
        didSet {
            if isFirstResponder {
                _ = self.becomeFirstResponder()
            }
        }
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        self.isSecureTextEntry = true
        button.isEnabled = true
    }

    override func textFieldDidEndEditing(_ textField: UITextField) {
        button.isEnabled = false
        self.isSecureTextEntry = true
        button.setImage(UIImage(systemName: "eye.fill"), for: .normal)
    }

    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }
    
    override func becomeFirstResponder() -> Bool {
        let success = super.becomeFirstResponder()
        if isSecureTextEntry, let text = self.text {
            self.text?.removeAll()
            self.insertText(text)
        }
        
//        configure()
        return success
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        super.rightViewRect(forBounds: bounds)
        return CGRect(
            x: (bounds.width * 88.1355) / 100,
            y: (bounds.height * 36.8983) / 100,
            width: (bounds.height * 43.8983) / 100,
            height: (bounds.height * 30.8983) / 100
        )
    }
    
    @objc
    func buttonTapped() {
        self.isSecureTextEntry.toggle()
        isEyeTapped.toggle()
        if isEyeTapped == true {
            button.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        } else {
            button.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        }
    }
}
