//
//  RegisterViewFullNameTextField.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 31.10.22.
//

import Foundation
import UIKit
import JVFloatLabeledTextField

class RegisterViewFullNameTextField: JVFloatLabeledTextField, UITextFieldDelegate {

    let bottomLine = CALayer()

    var path = UIBezierPath()
    var gradient = CAGradientLayer()

    func configure(superView: UIView) {
        self.placeholder = "Full Name"
        self.floatingLabelActiveTextColor = .gray
        self.tintColor = .yellow
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
        self.backgroundColor = .clear
        self.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.2).isActive = true
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    func addBottomLine() {
        guard self.frame.height != 0.0 && self.frame.width != 0.0 else { return }
        bottomLine.frame = CGRect(x: 0, y: (self.frame.height * 0.749) + 4, width: self.frame.width, height: 1)
        bottomLine.backgroundColor = UIColor(red: 0.871, green: 0.871, blue: 0.871, alpha: 1).cgColor
        self.borderStyle = .none
        self.layer.addSublayer(bottomLine)
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0))
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        addBottomLine()
        return textRect(forBounds: bounds)
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        path = UIBezierPath(rect: CGRect(x: 0, y: self.frame.height * 82 / 100, width: self.frame.width, height: 1))
        textField.clipsToBounds = false
        gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: textField.frame.size)
        gradient.colors = [
            UIColor(red: 0.565, green: 0.82, blue: 0.982, alpha: 1).cgColor,
            UIColor(red: 0.257, green: 0.702, blue: 0.846, alpha: 1).cgColor
        ]
        let shape = CAShapeLayer()
        shape.lineWidth = 1
        shape.path = path.cgPath
        shape.strokeColor = UIColor.orange.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape

        self.layer.insertSublayer(gradient, at: 0)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resignFirstResponder()
        self.layer.borderColor = UIColor(white: 1, alpha: 1).cgColor
        gradient.colors = [UIColor(red: 1, green: 1, blue: 1, alpha: 1)]
    }
}
