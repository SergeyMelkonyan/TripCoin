//
//  RegisterViewFieldsScrollView.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 23.10.22.
//

import Foundation
import UIKit

class RegisterViewFieldsScrollView: UIScrollView, UIScrollViewDelegate {
    
    let registerViewFieldsStack = RegisterViewFieldsStack()
    
    func setScrollViewConstraints(superView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            superView.topAnchor.constraint(equalTo: self.topAnchor, constant: -superView.frame.height * 23.44 / 100),
            superView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: -40),
            superView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 40),
            superView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: superView.frame.height * 23.44 / 100)
        ])
        
//        scrollViewDidScroll(self)
        self.addSubview(registerViewFieldsStack)
        registerViewFieldsStack.configure(superView: superView)
        registerViewFieldsStack.setStackConstraints(superView: self)
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let verticalIndicatorView = (scrollView.subviews[(scrollView.subviews.count - 1)].subviews[0])
//        verticalIndicatorView.backgroundColor = UIColor.yellow
//    }
}
