//
//  RegisterViewDescription.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 22.10.22.
//

import Foundation
import UIKit

class RegisterViewDescription: UILabel {
    func configure(superView: UIView) {
        self.text = "Registration is required so that you can start earning coins and redeem rewards.It'll only take a minute❤️"
        self.textColor = .black
        self.numberOfLines = 0
        let fontMultiplier: Double = 6 / 414
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
    }
}
