//
//  RegisterViewTitle.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 22.10.22.
//

import Foundation
import UIKit

class RegisterViewTitle: UILabel {
    func configure(superView: UIView) {
        self.text = "Register"
        self.textColor = .black
        let fontMultiplier: Double = 17 / 414
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont(name: "OpenSans-Bold", size: fontSize)
    }
}
