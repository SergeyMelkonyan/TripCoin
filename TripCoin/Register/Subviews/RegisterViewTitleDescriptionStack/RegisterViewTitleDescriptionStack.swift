//
//  RegisterViewTitleDescriptionStack.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 22.10.22.
//

import Foundation
import UIKit

class RegisterViewTitleDescriptionStack: UIStackView {

    let registerViewTitle = RegisterViewTitle()
    let registerViewDescription = RegisterViewDescription()

    func configure(superView: UIView) {
        self.axis = .vertical
        self.alignment = .leading
        self.distribution = .fillEqually
        self.spacing = 25

        self.addArrangedSubview(registerViewTitle)
        registerViewTitle.configure(superView: superView)

        self.addArrangedSubview(registerViewDescription)
        registerViewDescription.configure(superView: superView)

        self.translatesAutoresizingMaskIntoConstraints = false
    }

    func setStackConstraints(superView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            superView.topAnchor.constraint(
                equalTo: self.topAnchor,
                constant: -35
            ),
            superView.leadingAnchor.constraint(
                equalTo: self.leadingAnchor,
                constant: -40
            ),
            self.widthAnchor.constraint(
                equalTo: superView.widthAnchor,
                multiplier: 330 / 414
            ),
            self.heightAnchor.constraint(
                equalTo: superView.heightAnchor,
                multiplier: 110 / 717
            )
        ])
    }
}
