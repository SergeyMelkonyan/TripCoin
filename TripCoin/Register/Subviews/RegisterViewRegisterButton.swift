//
//  RegisterViewRegisterButton.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 23.10.22.
//

import Foundation
import UIKit

class RegisterViewRegisterButton: UIButton {
    
    let registerViewFieldStack = RegisterViewFieldsStack()
    
    func configure(superView: UIView) {
        self.setTitle("Register", for: .normal)
        self.setTitleColor(UIColor(red: 1, green: 1, blue: 1, alpha: 1.0), for: .normal)
        let fontMultiplier: Double = 8 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: fontSize)
        self.setContentHuggingPriority(.defaultLow, for: .horizontal)
        self.layer.cornerRadius = superView.frame.size.height / 35
        self.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        self.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @objc
    func buttonTapped() {
        print("sdfsdf")
    }
    
    func setConstraintsToButton(superView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        if superView.frame.width / superView.frame.height >= 0.6984 {
            NSLayoutConstraint.activate([
                
                superView.topAnchor.constraint(
                    equalTo: self.topAnchor,
                    constant: -(superView.frame.height * (520 / 736 * 100)) / 100
                ),
                
                self.centerXAnchor.constraint(
                    equalTo: superView.centerXAnchor
                ),
                
                self.widthAnchor.constraint(
                    equalTo: superView.widthAnchor,
                    multiplier: 0.9
                ),
                
                self.heightAnchor.constraint(
                    equalTo: superView.widthAnchor,
                    multiplier: 0.12
                )
            ])
        } else {
            NSLayoutConstraint.activate([
                
                superView.topAnchor.constraint(
                    equalTo: self.topAnchor,
                    constant: -(superView.frame.height * (535 / 736 * 100)) / 100
                ),
                
                self.centerXAnchor.constraint(
                    equalTo: superView.centerXAnchor
                ),
                
                self.widthAnchor.constraint(
                    equalTo: superView.widthAnchor,
                    multiplier: 0.9
                ),
                
                self.heightAnchor.constraint(
                    equalTo: superView.widthAnchor,
                    multiplier: 0.12
                )
            ])
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

    private lazy var gradientLayer: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [
            UIColor(red: 0.565, green: 0.82, blue: 0.902, alpha: 1).cgColor,
            UIColor(red: 0.257, green: 0.702, blue: 0.846, alpha: 1).cgColor
        ]
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.cornerRadius = self.layer.cornerRadius
        layer.insertSublayer(gradient, at: 0)
        return gradient
    }()
    
    convenience init() {
        self.init(type: .system)
    }
}
