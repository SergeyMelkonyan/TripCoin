//
//  RegisterViewController.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 20.10.22.
//

import Foundation
import UIKit

class RegisterViewController: UIViewController {

    let registerViewTitleDescriptionStack = RegisterViewTitleDescriptionStack()
    let registerViewFieldsScrollView = RegisterViewFieldsScrollView()
    let registerViewRegisterButton = RegisterViewRegisterButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white

        view.addSubview(registerViewTitleDescriptionStack)
        registerViewTitleDescriptionStack.configure(superView: view)
        registerViewTitleDescriptionStack.setStackConstraints(superView: view)
        
        view.addSubview(registerViewFieldsScrollView)
        registerViewFieldsScrollView.setScrollViewConstraints(superView: view)
//        registerViewFieldsScrollView.delegate = registerViewFieldsScrollView
        
        view.addSubview(registerViewRegisterButton)
        registerViewRegisterButton.configure(superView: view)
        registerViewRegisterButton.setConstraintsToButton(superView: view)
    }
}

extension RegisterViewController {
    override func updateViewConstraints() {
        self.view.frame.size.height = UIScreen.main.bounds.height - 60
        self.view.frame.origin.y = 60
        self.view.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        super.updateViewConstraints()
    }
}


