//
//  LoginViewSloganText.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class LoginViewSloganText: UILabel {
    func configure(superView: UIView) {
        self.text = "Discover Home."
        self.textColor = .white
        let fontMultiplier: Double = 9 / 414
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
        // Iphone 14 pro 0.4612
        // Iphone SE 0.5622
        // Ipad 0.6984
        self.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        self.textAlignment = .center
    }

    func setConstraints(superView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false

        if superView.frame.width / superView.frame.height > 0.5622 {
            NSLayoutConstraint.activate([
                superView.topAnchor.constraint(
                    equalTo: self.topAnchor,
                    constant: -(superView.frame.height * (265 / 736 * 100)) / 100
                ),
                superView.leadingAnchor.constraint(
                    equalTo: self.leadingAnchor,
                    constant: -(superView.frame.width * (142.97 / 414 * 100)) / 100
                ),
                self.widthAnchor.constraint(
                    equalTo: superView.widthAnchor,
                    multiplier: 140 / 414
                ),
                self.heightAnchor.constraint(
                    equalTo: superView.heightAnchor,
                    multiplier: 24 / 736
                ),
            ])
        } else {
            NSLayoutConstraint.activate([
                superView.topAnchor.constraint(
                    equalTo: self.topAnchor,
                    constant: -(superView.frame.height * (238 / 736 * 100)) / 100
                ),
                superView.leadingAnchor.constraint(
                    equalTo: self.leadingAnchor,
                    constant: -(superView.frame.width * (142.97 / 414 * 100)) / 100
                ),
                self.widthAnchor.constraint(
                    equalTo: superView.widthAnchor,
                    multiplier: 145 / 414
                ),
                self.heightAnchor.constraint(
                    equalTo: superView.heightAnchor,
                    multiplier: 24 / 736
                ),
            ])
        }
    }
}
