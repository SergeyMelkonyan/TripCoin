//
//  LoginViewBottomStack.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class LoginViewBottomStack: UIStackView {

    let loginViewForgotPassword = LoginViewForgotPassword()
    let loginViewSignInButton = LoginViewSignInButton()
    let loginViewNoAccountText = LoginViewNoAccountText()
    let loginViewRegisterButton = LoginViewRegisterButton()

    let haveNoAccountRegisterButtonStack = HaveNoAccountRegisterStack()
    let forgotPassSignInStack = ForgotPassSignInStack()

    func configure(superView: UIView) {
        self.axis = .vertical
        self.alignment = .center
        self.distribution = .fillEqually

        self.addArrangedSubview(forgotPassSignInStack)
        forgotPassSignInStack.configure(superView: superView)

        self.addArrangedSubview(haveNoAccountRegisterButtonStack)
        haveNoAccountRegisterButtonStack.configure(superView: superView)

        self.translatesAutoresizingMaskIntoConstraints = false
    }

    func setConstraintsToStack(superView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            superView.topAnchor.constraint(
                equalTo: self.topAnchor,
                constant: -(superView.frame.height * (519 / 736 * 100)) / 100
            ),
            superView.bottomAnchor.constraint(
                equalTo: self.bottomAnchor,
                constant: (superView.frame.height * (40 / 736 * 100)) / 100
            ),
            superView.centerXAnchor.constraint(
                equalTo: self.centerXAnchor
            )
        ])
    }
}
