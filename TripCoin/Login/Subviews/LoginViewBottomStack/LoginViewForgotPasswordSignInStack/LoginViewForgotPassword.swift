//
//  LoginViewForgotPassword.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class LoginViewForgotPassword: UIButton {
    func configure(superView: UIView) {
        self.setTitle("Forgot Password?", for: .normal)
        self.setTitleColor(UIColor(red: 1, green: 1, blue: 1, alpha: 0.5), for: .normal)
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.titleLabel?.font = UIFont.setCustomFontWith(size: fontSize)
        self.titleLabel?.textAlignment = .right
        self.contentHorizontalAlignment = .right
        self.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    convenience init() {
        self.init(type: .system)
    }
}
