//
//  ForgotPassSignInStack.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 21.10.22.
//

import Foundation
import UIKit

class ForgotPassSignInStack: UIStackView {

    let loginViewForgotPassword = LoginViewForgotPassword()
    let loginViewSignInButton = LoginViewSignInButton()

    func configure(superView: UIView) {
        self.axis = .vertical
        self.alignment = .fill
        self.distribution = .fillEqually
        self.translatesAutoresizingMaskIntoConstraints = false

        self.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)

        self.addArrangedSubview(loginViewForgotPassword)
        loginViewForgotPassword.configure(superView: superView)

        self.addArrangedSubview(loginViewSignInButton)
        loginViewSignInButton.configure(superView: superView)
    }
}
