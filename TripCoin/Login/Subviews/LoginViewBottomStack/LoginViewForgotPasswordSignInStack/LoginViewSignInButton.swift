//
//  LoginViewSignInButton.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class LoginViewSignInButton: UIButton {

    func configure(superView: UIView) {
        self.setTitle("Sign In", for: .normal)
        self.setTitleColor(.white, for: .normal)
        let fontMultiplier: Double = 8 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.titleLabel?.font = UIFont.setCustomFontWith(size: fontSize)
        self.widthAnchor.constraint(equalTo: superView.widthAnchor, multiplier: 371 / 414).isActive = true
        self.layer.cornerRadius = superView.frame.size.height / 31
        self.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    convenience init() {
        self.init(type: .system)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

    private lazy var gradientLayer: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [
            UIColor(red: 0.565, green: 0.82, blue: 0.902, alpha: 1).cgColor,
            UIColor(red: 0.257, green: 0.702, blue: 0.846, alpha: 1).cgColor
        ]
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.cornerRadius = self.layer.cornerRadius
        layer.insertSublayer(gradient, at: 0)
        return gradient
    }()
}
