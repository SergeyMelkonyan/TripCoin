//
//  HaveNoAccountAndRegisterStack.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class HaveNoAccountRegisterStack: UIStackView {

    let loginHaveNoAccountText = LoginViewNoAccountText()
    let loginViewRegisterButton = LoginViewRegisterButton()

    // Iphone SE 375 : 667 0.5622
    // Ipone 14 pro 393 : 852 0.4612
    // IPhone 14 pro max 430 : 932 0.461
    // IPhone 8 375 : 667 0.5622

    func configure(superView: UIView) {
        self.axis = .horizontal
        self.alignment = .fill
        self.distribution = .fill
        self.spacing = 8
        self.translatesAutoresizingMaskIntoConstraints = false

        self.addArrangedSubview(loginHaveNoAccountText)
        loginHaveNoAccountText.configure(superView: superView)

        self.addArrangedSubview(loginViewRegisterButton)
        loginViewRegisterButton.configure(superView: superView)
    }
}
