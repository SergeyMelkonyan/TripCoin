//
//  LoginViewNoAccountText.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class LoginViewNoAccountText: UILabel {
    func configure(superView: UIView) {
        self.text = "Don't have an account?"
        self.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.8)
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
        self.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        self.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}
