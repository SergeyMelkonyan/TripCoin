//
//  LoginViewRegisterButton.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

protocol LoginViewRegisterButtonDelegate: AnyObject {
    func needToPresent()
}

class LoginViewRegisterButton: UIButton {

    weak static var delegate: LoginViewRegisterButtonDelegate?
    
    func configure(superView: UIView) {
        self.setTitle("Register", for: .normal)
        self.setTitleColor(UIColor(red: 1, green: 1, blue: 1, alpha: 1.0), for: .normal)
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: fontSize)
        self.setContentHuggingPriority(.defaultLow, for: .horizontal)
        self.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }

    convenience init() {
        self.init(type: .system)
    }

    @objc
    func buttonTapped() {
        LoginViewRegisterButton.delegate?.needToPresent()
    }
}


