//
//  LoginBackgroundImage.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class LoginBackgroundImage: UIImageView {
    func configure(superView: UIView) {
        self.image = UIImage(named: "login")
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    func setConstraints(superView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false

        self.contentMode = .scaleAspectFill
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(
                equalTo: superView.topAnchor,
                constant: 0
            ),
            self.leadingAnchor.constraint(
                equalTo: superView.leadingAnchor,
                constant: 0
            ),
            self.heightAnchor.constraint(
                equalTo: superView.heightAnchor,
                multiplier: 1.0
            ),
            self.widthAnchor.constraint(
                equalTo: superView.heightAnchor,
                multiplier: superView.frame.width / superView.frame.height
            )
        ])
    }
}
