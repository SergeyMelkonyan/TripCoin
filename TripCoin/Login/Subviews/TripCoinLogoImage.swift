//
//  TripCoinLogoImage.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class TripCoinLogoImage: UIImageView {
    func configure(superView: UIView) {
        self.image = UIImage(named: "tripCoinLogo")
        self.contentMode = .scaleAspectFill
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    func setConstraints(superView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            superView.topAnchor.constraint(
                equalTo: self.safeAreaLayoutGuide.topAnchor,
                constant: -(superView.frame.height * (56.3 / 736 * 100)) / 100
            ),
            superView.leadingAnchor.constraint(
                equalTo: self.safeAreaLayoutGuide.leadingAnchor,
                constant: -(superView.frame.width * (87.71 / 414 * 100)) / 100
            ),
            self.widthAnchor.constraint(
                equalTo: superView.widthAnchor,
                multiplier: 273.8 / 414
            ),
            self.heightAnchor.constraint(
                equalTo: self.widthAnchor,
                multiplier: 183.56 / 273.58
            ),
        ])
    }
}
