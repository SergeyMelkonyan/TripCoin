//
//  LoginViewSloganFieldsStack.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class LoginViewFieldsStack: UIStackView {

    // MARK: - Subviews
    let loginViewSloganText = LoginViewSloganText()
    let loginViewEmailTextField = LoginViewEmailTextField()
    let loginViewPasswordTextField = LoginViewPasswordTextField()

    func configure(superView: UIView) {
        self.axis = .vertical
        self.alignment = .fill
        self.distribution = .fillEqually
        self.spacing = 10

        self.addArrangedSubview(loginViewEmailTextField)
        loginViewEmailTextField.configure(superView: superView)
        loginViewEmailTextField.setCornerRadius(radius: 15)
        loginViewEmailTextField.delegate = loginViewEmailTextField

        self.addArrangedSubview(loginViewPasswordTextField)
        loginViewPasswordTextField.configure()
        loginViewPasswordTextField.setTextFieldSizes(superView: superView)
        loginViewPasswordTextField.setCornerRadius(radius: 15)
        loginViewPasswordTextField.delegate = loginViewPasswordTextField

        self.translatesAutoresizingMaskIntoConstraints = false
    }

    func setConstraintsToStack(superView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false

        // TODO: 
        NSLayoutConstraint.activate([
            superView.topAnchor.constraint(
                equalTo: self.topAnchor,
                constant: -(superView.frame.height * (300 / 736 * 100)) / 100
            ),
            superView.leadingAnchor.constraint(
                equalTo: self.leadingAnchor,
                constant: -(superView.frame.width * (21.75 / 414 * 100)) / 100
            ),
            superView.trailingAnchor.constraint(
                equalTo: self.trailingAnchor,
                constant: (superView.frame.width * (21.25 / 414 * 100)) / 100
            ),
            superView.bottomAnchor.constraint(
                equalTo: self.bottomAnchor,
                constant: (superView.frame.height * (336 / 736 * 100)) / 100
            ),
        ])
    }
}
