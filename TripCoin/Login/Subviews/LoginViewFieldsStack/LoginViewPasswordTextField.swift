//
//  LoginViewPasswordTextField.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import Foundation
import UIKit

class LoginViewPasswordTextField: UITextField, UITextFieldDelegate {

    private var isPasswordTextFieldSelected = false
    
    var path = UIBezierPath()
    var gradient = CAGradientLayer()

    func setTextFieldSizes(superView: UIView) {
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
    }

    func configure() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7).cgColor
        self.isSecureTextEntry = true
        self.placeholder = "Password"
        if let placeholder = self.placeholder, !isPasswordTextFieldSelected {
            self.attributedPlaceholder = NSAttributedString(
                string: placeholder,
                attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)]
            )
        }
        self.textColor = .white
        self.enablePasswordToggle()
        self.textContentType = .oneTimeCode
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        isPasswordTextFieldSelected = true
        self.isSecureTextEntry = true
        configure()
        
        self.layer.borderColor = UIColor(white: 1, alpha: 0).cgColor
        let lineWidth: CGFloat = 1
        let rect = bounds.insetBy(dx: lineWidth / 3, dy: lineWidth / 3)
        path = UIBezierPath(roundedRect: rect, cornerRadius: 15)
        textField.clipsToBounds = false
        gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: textField.frame.size)
        gradient.colors = [
            UIColor(red: 0.565, green: 0.82, blue: 0.902, alpha: 1).cgColor,
            UIColor(red: 0.257, green: 0.702, blue: 0.846, alpha: 1).cgColor
        ]
        let shape = CAShapeLayer()
        shape.lineWidth = 1
        shape.path = path.cgPath
        shape.strokeColor = UIColor.orange.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape

        textField.layer.addSublayer(gradient)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        isPasswordTextFieldSelected = false
        configure()
        self.layer.borderColor = UIColor(white: 1, alpha: 0.7).cgColor
        gradient.colors = [UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)]
    }

    func setCornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }

    override var isSecureTextEntry: Bool {
        didSet {
            if isFirstResponder {
                _ = becomeFirstResponder()
            }
        }
    }

    override func becomeFirstResponder() -> Bool {
        let success = super.becomeFirstResponder()
        if isSecureTextEntry, let text = self.text {
            self.text?.removeAll()
            self.insertText(text)
        }

        return success
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.layer.borderColor = UIColor(white: 1, alpha: 0.7).cgColor
        gradient.colors = [UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)]
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }
}
