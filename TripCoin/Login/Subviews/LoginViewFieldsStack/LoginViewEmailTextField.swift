//
//  LoginViewEmailTextField.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.

import Foundation
import UIKit
import QuartzCore

class LoginViewEmailTextField: UITextField, UITextFieldDelegate {
    
    var path = UIBezierPath()
    var gradient = CAGradientLayer()
    
    func configure(superView: UIView) {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7).cgColor
        let fontMultiplier: Double = 6 / 302
        let fontSize = CGFloat(superView.frame.height * CGFloat(fontMultiplier))
        self.font = UIFont.setCustomFontWith(size: fontSize)
        self.keyboardType = .emailAddress
        self.placeholder = "E-mail"
        self.layer.masksToBounds = true
        self.tintColor = .yellow
        if let placeholder = self.placeholder {
            self.attributedPlaceholder = NSAttributedString(
                string: placeholder,
                attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)]
            )
        }

        self.clipsToBounds = false
        self.textColor = .white
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.size.height / 2
    }

    func setCornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0))
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.layer.borderColor = UIColor(white: 1, alpha: 0).cgColor
        let lineWidth: CGFloat = 1
        let rect = bounds.insetBy(dx: lineWidth / 3, dy: lineWidth / 3)
        path = UIBezierPath(roundedRect: rect, cornerRadius: 15)
        textField.clipsToBounds = false
        gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: textField.frame.size)
        gradient.colors = [
            UIColor(red: 0.565, green: 0.82, blue: 0.902, alpha: 1).cgColor,
            UIColor(red: 0.257, green: 0.702, blue: 0.846, alpha: 1).cgColor
        ]
        let shape = CAShapeLayer()
        shape.lineWidth = 1
        shape.path = path.cgPath
        shape.strokeColor = UIColor.orange.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        textField.layer.addSublayer(gradient)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.layer.borderColor = UIColor(white: 1, alpha: 0.7).cgColor
        gradient.colors = [UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)]
    }
}
