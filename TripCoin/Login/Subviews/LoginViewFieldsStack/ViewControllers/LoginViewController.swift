//
//  ViewController.swift
//  TripCoin
//
//  Created by Sergey Melkonyan on 19.10.22.
//

import UIKit

class LoginViewController: UIViewController {

    // MARK: - Subviews

    let loginBackgroundImage = LoginBackgroundImage()
    let tripCoinLogoImage = TripCoinLogoImage()
    let loginViewSloganFieldsStack = LoginViewFieldsStack()
    let loginViewBottomStack = LoginViewBottomStack()
    let loginViewSloganText = LoginViewSloganText()

    // MARK: - Lifecycle
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("dsf")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        LoginViewRegisterButton.delegate = self

        view.insertSubview(loginBackgroundImage, at: 0)
        loginBackgroundImage.configure(superView: view)
        loginBackgroundImage.setConstraints(superView: view)

        view.addSubview(tripCoinLogoImage)
        tripCoinLogoImage.configure(superView: view)
        tripCoinLogoImage.setConstraints(superView: view)

        view.addSubview(loginViewSloganFieldsStack)
        loginViewSloganFieldsStack.configure(superView: view)
        loginViewSloganFieldsStack.setConstraintsToStack(superView: view)

        view.addSubview(loginViewBottomStack)
        loginViewBottomStack.configure(superView: view)
        loginViewBottomStack.setConstraintsToStack(superView: view)

        view.addSubview(loginViewSloganText)
        loginViewSloganText.configure(superView: view)
        loginViewSloganText.setConstraints(superView: view)
    }

    @objc
    func didTapView(sender: UITapGestureRecognizer) {
      print("View Tapped")
    }
}


extension LoginViewController: LoginViewRegisterButtonDelegate {
    func needToPresent() {
        let registerViewController = RegisterViewController()
        self.present(registerViewController, animated: true)
    }
}
